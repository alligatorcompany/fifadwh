# Initialize

All you need is a kubectl config connected to a kubernetes.

And a namespace with an 
- exasol service listening on 8563
- and a dvb service listening on 8000

You can change those in okteto.yml:
forward:
  - 8563:exasol:8563
  - 8001:dvb:8000

Environment variables for default profiles.yml connectivity in dbt:
  - DBT_USER=sys
  - DBT_PASS=start123
  - DBT_PROFILES_DIR=.
  - EXASOL_SERVICE_HOST=localhost
  - EXASOL_SERVICE_PORT=8563

# FIFA sources
https://www.kaggle.com/hugomathien/soccer