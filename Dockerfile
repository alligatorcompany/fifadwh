FROM alligatorcompany/acs-workbench:1.0.18 as dvb
ARG DVB_USER
ARG DVB_PASS
ARG DVB_URL
ENV Login_Username ${DVB_USER}
ENV Login_Password ${DVB_PASS}
ENV Base_URL ${DVB_URL}
ADD 01_dv /wks/01_dv
WORKDIR /wks
RUN /wks/01_dv/dvb/rollout.sh

FROM alligatorcompany/acs-workbench:1.0.18 as dbt
ARG DBT_PROFILES_DIR
ARG DBT_USER
ARG DBT_PASS
ARG DBT_VERSION_UTILS
ARG DBT_VERSION_EXASOL_UTILS
ARG DBT_VERSION_GDWH
ARG EXASOL_SERVICE_HOST
ARG EXASOL_SERVICE_PORT
ENV DBT_PROFILES_DIR ${DBT_PROFILES_DIR}
ENV DBT_USER ${DBT_USER}
ENV DBT_PASS ${DBT_PASS}
ENV DBT_VERSION_UTILS ${DBT_VERSION_UTILS}
ENV DBT_VERSION_EXASOL_UTILS ${DBT_VERSION_EXASOL_UTILS}
ENV DBT_VERSION_GDWH ${DBT_VERSION_GDWH}
ENV DBT_BASE_DIR /wks
ENV EXASOL_SERVICE_HOST ${EXASOL_SERVICE_HOST}
ENV EXASOL_SERVICE_PORT ${EXASOL_SERVICE_PORT}
ADD 00_src /wks/00_src
ADD 02_app /wks/02_app
ADD 04_sbx /wks/04_sbx
ADD Makefile /wks/
WORKDIR /wks/
RUN make
WORKDIR /wks/02_app/app/
RUN dbt deps && dbt docs generate

FROM peaceiris/hugo:v0.92.1-full as hugo
RUN apk add --update-cache --no-cache asciidoctor
COPY ./docs/ /site
WORKDIR /site
RUN hugo
RUN git clone https://gitlab.com/alligatorcompany/acs-dmstack-doc.git /acs-docs
WORKDIR /acs-docs
RUN git submodule update --init && hugo -b /acs-docs/
RUN mkdir /site/public/acs-docs/
RUN cp -r /acs-docs/public/* /site/public/acs-docs/
RUN cp -r /acs-docs/static/* /site/public/

FROM nginx:alpine as docs
COPY --from=hugo /site/public /usr/share/nginx/html
COPY --from=dbt /wks/02_app/app/target/ /usr/share/nginx/html/catalog/
COPY 00_src /code/00_src
COPY 01_dv /code/01_dv
COPY 02_app /code/02_app
COPY 04_sbx /code/04_sbx
COPY dwh /code/dwh
COPY ingest /code/ingest
COPY wf /code/wf
WORKDIR /usr/share/nginx/html
