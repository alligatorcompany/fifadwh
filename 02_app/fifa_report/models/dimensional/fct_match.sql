with matches as (

    select * from {{ ref('stg_match') }}

)

select
    [HKTeamHome],
    [HKTeamAway],
    [HKLeague],
    month("date") as month_no,
    season,
    goal,
    away_team_goal,
    home_team_goal

from matches
