
{{
    config(
        materialized='table'
    )
}}

select 
    source_system,
    interface_schema,
    interface_name,

    'id' as index_col,

    HASHTYPE_MD5(
        json_value(payload, '$.id' )  || '^~|'
    ) as bk, 

    payload

from (
    select yyypsa.importjsonfrom('fifa','public','matches') from dual
)